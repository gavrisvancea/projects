package JUnit;
import static org.junit.Assert.*;

import Polynomial.Calculator;
import Polynomial.Polynomial;
import org.junit.*;

public class JUnit {

    private static Calculator c;
    private static Polynomial a;
    private static Polynomial b;

    private static int nOfTests=0;
    private static int nOfSTests=0;

    public JUnit()
    {
        System.out.println("Constructor before testing:");
    }
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

        Calculator c = new Calculator();
        Polynomial a = new Polynomial();
        Polynomial b = new Polynomial();
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {

        System.out.println("Number of executed tests: " + nOfTests + "\n "+ "Number of successfully executed tests: " + nOfSTests + "");
    }

    @Before
    public void setUp() throws Exception {
        System.out.println("New test!");
        nOfTests++;
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("Current test finished!");
    }


    @Test
    public void testAdunare() {
        a = new Polynomial("6x^3+2x^2+5");
        b = new Polynomial("-4x^3-5x^2+x-2");
        c = new Calculator(a,b);

        Polynomial rez = c.adunare();

        assertNotNull(rez.toString());
        assertEquals(rez.toString(),"2.0X^3+-3.0X^2+1.0X+3.0");
        nOfSTests++;
    }
    @Test
    public void testScadere() {
        a = new Polynomial("6x^3+2x^2+5");
        b = new Polynomial("-4x^3-5x^2+x-2");
        c = new Calculator(a,b);

        Polynomial rez = c.scadere();

        assertNotNull(rez.toString());
        assertEquals(rez.toString(),"10.0X^3+7.0X^2+-1.0X+7.0");
        nOfSTests++;
    }

    @Test
    public void testInmultire() {

        a = new Polynomial("x+2");
        b = new Polynomial("x+1");
        c = new Calculator(a,b);

        Polynomial rez = c.inmultire();

        assertNotNull(rez.toString());
        assertEquals(rez.toString(),"1.0X^2+3.0X+2.0");
        nOfSTests++;
    }
    @Test
    public void testImpartire() {

        a = new Polynomial("6x^2+x-1");
        b = new Polynomial("2x-3");
        c = new Calculator(a,b);

        Polynomial[] rez = c.impartire();

        assertNotNull(rez[0].toString());
        assertEquals(rez[0].toString(),"3.0X+5.0");
        assertNotNull(rez[1].toString());
        assertEquals(rez[1].toString(),"14.0");
        nOfSTests++;
    }
    @Test
    public void testDerivare() {
        a = new Polynomial("3x^2+2x");
        b = new Polynomial("3x^2+1");
        c = new Calculator(a,b);

        Polynomial rez = c.derivare();

        assertNotNull(rez.toString());
        assertEquals(rez.toString(),"6.0X+2.0");
        nOfSTests++;
    }

    @Test
    public void testIntegrare() {

        a = new Polynomial("3x^2+2x");
        b = new Polynomial("3x^2+1");
        c = new Calculator(a,b);

        Polynomial rez = c.integrare();

        assertNotNull(rez.toString());
        assertEquals(rez.toString(),"1.0X^3+1.0X^2");

        nOfSTests++;
    }
}
