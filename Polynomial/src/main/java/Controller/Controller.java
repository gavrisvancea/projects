package Controller;
import Polynomial.*;
import View.View;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller {

    private View frame;

    public Controller(View v)
    {
        this.frame = v;

        frame.getB1().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String x = frame.getTf1().getText();
                String y = frame.getTf2().getText();

                Polynomial a = new Polynomial(x);
                Polynomial b = new Polynomial(y);

                Calculator calc = new Calculator(a,b);

                Polynomial rez = calc.adunare();

                String out = rez.toString();
                frame.getTf3().setEditable(true);
                frame.getTf3().setText(out);

            }
        });

        frame.getB2().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String x = frame.getTf1().getText();
                String y = frame.getTf2().getText();

                Polynomial a = new Polynomial(x);
                Polynomial b = new Polynomial(y);

                Calculator calc = new Calculator(a,b);
                Polynomial rez = calc.scadere();

                frame.getTf3().setText(null);

                String out = rez.toString();

                frame.getTf3().setEditable(true);
                frame.getTf3().setText(out);
            }
        });


        frame.getB3().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String x = frame.getTf1().getText();
                String y = frame.getTf2().getText();

                Polynomial a = new Polynomial(x);
                Polynomial b = new Polynomial(y);

                Calculator calc = new Calculator(a,b);
                Polynomial rez = calc.inmultire();

                frame.getTf3().setText(null);

                String out = rez.toString();

                frame.getTf3().setEditable(true);
                frame.getTf3().setText(out);
            }
        });

        frame.getB4().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String x = frame.getTf1().getText();
                String y = frame.getTf2().getText();

                Polynomial a = new Polynomial(x);
                Polynomial b = new Polynomial(y);

                Calculator calc = new Calculator(a,b);
                Polynomial[] rez = calc.impartire();

                frame.getTf3().setText(null);
                frame.getTf4().setText(null);

                String out = rez[0].toString();
                String out2 = rez[1].toString();

                frame.getTf3().setEditable(true);
                frame.getTf3().setText(out);

                frame.getTf4().setEditable(true);
                frame.getTf4().setText(out2);
            }
        });

        frame.getB5().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String x = frame.getTf1().getText();

                Polynomial a = new Polynomial(x);
                Polynomial b = new Polynomial();

                Calculator calc = new Calculator(a,b);
                Polynomial rez = calc.derivare();

                frame.getTf3().setText(null);

                String out = rez.toString();

                frame.getTf3().setEditable(true);
                frame.getTf3().setText(out);
            }
        });

        frame.getB6().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String x = frame.getTf1().getText();

                Polynomial a = new Polynomial(x);
                Polynomial b = new Polynomial();

                Calculator calc = new Calculator(a,b);
                Polynomial rez = calc.integrare();

                frame.getTf3().setText(null);

                String out = rez.toString();

                frame.getTf3().setEditable(true);
                frame.getTf3().setText(out);
            }
        });
    }

    public static void main(String args[]) {
        View frame = new View();

        Controller c = new Controller(frame);

    }

}
