package View;
import javax.swing.*;
import java.awt.*;

public class View{

    private JFrame frame = new JFrame("Calculator polinoame");
	private Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    private JPanel panel = new JPanel();

	private JLabel l1 = new JLabel("A: ");
	private JTextArea tf1 = new JTextArea();

    private JLabel l2 = new JLabel("B: ");
	private JTextArea tf2 = new JTextArea();

    private JLabel l3 = new JLabel("Rezultat(Cât): ");
	private JTextArea tf3 = new JTextArea();

    private JLabel l4 = new JLabel("Rest: ");
	private JTextArea tf4 = new JTextArea();

	private JButton b1 = new JButton("Adunare");
	private JButton b2 = new JButton("Scadere");
	private JButton b3 = new JButton("Inmultire");
	private JButton b4 = new JButton("Impartire");
	private JButton b5 = new JButton("Derivare A");
	private JButton b6 = new JButton("Integrare A");
	private JLabel format = new JLabel("format: coefx^exp; pt coef = (-)1: (-)x^exp; pt exp = 1: coef");

	public JTextArea getTf1() {
		return tf1;
	}

	public void setTf1(JTextArea tf1) {
		this.tf1 = tf1;
	}

	public JTextArea getTf2() {
		return tf2;
	}

	public void setTf2(JTextArea tf2) {
		this.tf2 = tf2;
	}

	public JTextArea getTf3() {
		return tf3;
	}

	public void setTf3(JTextArea tf3) {
		this.tf3 = tf3;
	}

	public JTextArea getTf4() {
		return tf4;
	}

	public void setTf4(JTextArea tf4) {
		this.tf4 = tf4;
	}

	public JButton getB1() {
		return b1;
	}

	public void setB1(JButton b1) {
		this.b1 = b1;
	}

	public JButton getB2() {
		return b2;
	}

	public void setB2(JButton b2) {
		this.b2 = b2;
	}

	public JButton getB3() {
		return b3;
	}

	public void setB3(JButton b3) {
		this.b3 = b3;
	}

	public JButton getB4() {
		return b4;
	}

	public void setB4(JButton b4) {
		this.b4 = b4;
	}

	public JButton getB5() {
		return b5;
	}

	public void setB5(JButton b5) {
		this.b5 = b5;
	}

	public JButton getB6() {
		return b6;
	}

	public void setB6(JButton b6) {
		this.b6 = b6;
	}

	public View(){

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(625, 380);
		int x = (int) ((d.getWidth() - frame.getWidth()) / 2);
		int y = (int) ((d.getHeight() - frame.getHeight()) / 2);
		frame.setLocation(x, y); // afisare la mijloc
        panel.setLayout(null);

        // label 1 + text area 1: Polinom A
        panel.add(l1);
		l1.setBounds(5,10,100,25);
		panel.add(tf1);
		tf1.setBounds(120, 10, 300, 20);

        //label 2 + text area 2: Polinom B
        panel.add(l2);
		l2.setBounds(5,50,100,25);
		panel.add(tf2);
		tf2.setBounds(120, 50, 300, 20);

        //label 3 + text area 3: Rezultat
        l3.setForeground(Color.blue);
        panel.add(l3);
		l3.setBounds(5,90,100,25);
		panel.add(tf3);
		tf3.setBounds(120, 90, 300, 20);

        //label 4 + text area 4: Rest
        l4.setForeground(Color.red);
        panel.add(l4);
		l4.setBounds(5,130,100,25);
		panel.add(tf4);
		tf4.setBounds(120, 130, 300, 20);

        //buttons

        panel.add(b1);
		b1.setBounds(5,200,100,35);

		panel.add(b2);
		b2.setBounds(100,200,100,35);

		panel.add(b3);
		b3.setBounds(200,200,105,35);

		panel.add(b4);
		b4.setBounds(300,200,105,35);

		panel.add(b5);
		b5.setBounds(5,250,100,35);

		panel.add(b6);
		b6.setBounds(100,250,100,35);

		panel.add(format);
		format.setBounds(5, 300, 400, 25);

        frame.setContentPane(panel);
        frame.setVisible(true);
    }
}



