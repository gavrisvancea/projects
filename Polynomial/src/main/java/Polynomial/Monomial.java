package Polynomial;

public class Monomial implements Comparable<Monomial>{

    private double coeficient;
    private int exponent;

    public Monomial() {

        this.exponent = 0;
        this.coeficient = 0;
    }


    Monomial(int exp, double coef)
    {
        this.coeficient = coef;
        this.exponent = exp;
    }

    public double getCoef()
    {
        return this.coeficient;
    }

    public void setCoef(double co){
        this.coeficient = co;
    }

    public int getExp()
    {
        return this.exponent;
    }

    public void setExp(int exp){
        this.exponent = exp;
    }

    public String toString() {
        if(this.exponent == 0)
            return this.coeficient + "";
        else if(this.exponent == 1)
            return this.coeficient + "X";
        else if(this.exponent == 1){
                return "X";
        }
         else{
             return this.coeficient + "X^" + this.exponent;
        }
    }

    public boolean equals(Monomial m) {
        return this.exponent == m.getExp() && this.coeficient == m.getCoef();
    }

    public Monomial inmultire(Monomial m){
        Monomial rez = new Monomial();

        rez.setCoef(this.getCoef() * m.getCoef());
        rez.setExp(this.getExp() + m.getExp());

        return rez;
    }

    public Monomial getNegative(){
        Monomial aux = new Monomial(this.exponent, this.coeficient * (-1));
        return aux;
    }

    public void adunare(Monomial m){
        this.coeficient = this.getCoef() + m.getCoef();

    }
    @Override
    public int compareTo(Monomial o) {
        return -(this.exponent-o.exponent);
    }
}
