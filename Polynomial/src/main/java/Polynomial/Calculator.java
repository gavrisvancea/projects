package Polynomial;
import java.util.*;

public class Calculator {

    private Polynomial a;
    private Polynomial b;

    public Calculator(){
        a = new Polynomial();
        b = new Polynomial();
    }

    public Calculator(Polynomial a, Polynomial b) {
        this.a = a;
        this.b = b;
    }

    public Polynomial adunare() {
        Polynomial rez = new Polynomial();
        Polynomial ca = new Polynomial(a);
        Polynomial cb = new Polynomial(b);
        Polynomial aux1 = new Polynomial();
        for(Monomial i: ca.getList())
            for (Monomial j : cb.getList())
                if (i.getExp() == j.getExp()) {
                    Monomial aux = new Monomial(i.getExp(), i.getCoef() + j.getCoef());
                    aux1.addTerm(i);
                    aux1.addTerm(j);
                    rez.addTerm(aux);
                }
        for(Monomial i: aux1.getList()){
            ca.getList().remove(i);
            cb.getList().remove(i);
        }
        for(Monomial i: ca.getList()){
            cb.addTerm(i);
        }
        for(Monomial i: cb.getList()){
            rez.addTerm(i);
        }
        Collections.sort(rez.getList());
        return rez;
    }

    public Polynomial scadere() {
        Polynomial rez = new Polynomial();
        Polynomial ca = new Polynomial(a);
        Polynomial cb = new Polynomial(b);
        Polynomial aux1 = new Polynomial();
        for(Monomial i: ca.getList())
            for (Monomial j : cb.getList())
                if (i.getExp() == j.getExp()) {
                    Monomial aux = new Monomial(i.getExp(), i.getCoef() - j.getCoef());
                    rez.addTerm(aux);
                    aux1.addTerm(i);
                    aux1.addTerm(j);
                }
        for(Monomial i: aux1.getList()){
            ca.getList().remove(i);
            cb.getList().remove(i);
        }
        for(Monomial i: ca.getList()){
            rez.addTerm(i);
        }
        for(Monomial i: cb.getList()) {
            i.setCoef(-i.getCoef());
        }
        for(Monomial i: cb.getList()){
            rez.addTerm(i);
        }
        Collections.sort(rez.getList());
        return rez;
    }

    public Polynomial inmultire(){
        Polynomial rez = new Polynomial();
        for(Monomial i:a.getList()){
            for(Monomial j:b.getList()){
                Monomial aux = new Monomial(i.getExp() + j.getExp(), i.getCoef() * j.getCoef());
                Polynomial aux2 = new Polynomial(rez);
                int ver = 0;
                for(Monomial k: aux2.getList()){
                    if(k.getExp() == aux.getExp()){
                        k.adunare(aux);
                        ver = 1;
                    }
                }
                if(ver == 0){
                    rez.addTerm(aux);
                }
            }
        }
        Collections.sort(rez.getList());
        return rez;
    }

    public Polynomial[] impartire(){
        Polynomial[] rez = new Polynomial[2];
        Polynomial cat = new Polynomial();
        Polynomial rest = new Polynomial(this.a);

        if(this.b != null){
            while(rest != null && rest.getGrade() >= this.b.getGrade()){
                Monomial term = new Monomial(rest.getTermMaxGrade().getExp() - this.b.getTermMaxGrade().getExp(), rest.getTermMaxGrade().getCoef() / this.b.getTermMaxGrade().getCoef());
                cat.addTerm(term);
                Polynomial t = new Polynomial(term);
                Calculator c = new Calculator(t, this.b);
                Polynomial calc = c.inmultire();
                Polynomial aux = new Polynomial(rest);
                Calculator c2 = new Calculator(rest, calc);
                rest = c2.scadere();
            }
        }
        rez[0] = cat;
        rez[1] = rest;
        return rez;
    }

    public Polynomial derivare(){

        Polynomial rez = new Polynomial();

        for(Monomial i:a.getList()){
            Monomial aux = new Monomial(i.getExp() - 1, i.getCoef() * i.getExp());
            if(aux.getCoef() != 0) {
                rez.addTerm(aux);
            }
        }
        Collections.sort(rez.getList());
        return rez;
    }

    public Polynomial integrare(){
        Polynomial rez = new Polynomial();

        for(Monomial i: a.getList()){
            Monomial aux = new Monomial(i.getExp() + 1, i.getCoef()/(i.getExp() + 1));
            rez.addTerm(aux);
        }
        Collections.sort(rez.getList());
        return rez;
    }

    //Test intr un main
    /*
    public static void main(String args[]){

        System.out.println("Te iubesc! <3");

        String pol1 = "x^4+10x^2+3x+5";
        String pol2 = "x^2";

        Polynomial a = new Polynomial(pol1);
        Polynomial b = new Polynomial(pol2);
        Calculator calc = new Calculator(a,b);//

        //Polynomial rez = calc.adunare();
        System.out.println(a);
        System.out.println(b);
        //System.out.println(rez);

        Polynomial rez2 = calc.scadere();
        System.out.println(rez2);

        Polynomial rez3 = calc.inmultire();
        System.out.println(rez3);

        Polynomial rez4 = calc.derivare();
        System.out.println(rez4);

        Polynomial rez5 = calc.integrare();
        System.out.println(rez5);



        Polynomial[] rez = calc.impartire();

        for(Polynomial p: rez){
            System.out.println(p);
        }


        System.out.println("cat: " + rez[0]);
        System.out.println("rest: " + rez[1]);
   }
        */


}
