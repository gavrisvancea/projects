package Polynomial;
import java.util.*;

import java.util.ArrayList;
import java.util.regex.Pattern;

public class Polynomial {

    private ArrayList<Monomial> list;

    public Polynomial() {
        list = new ArrayList<Monomial>();
    }

    public Polynomial(Polynomial a){
        list =(ArrayList<Monomial>)a.list.clone();
    }

    public Polynomial(String pol) {
        list = new ArrayList<Monomial>();

        double c = 0;
        int p = 0;
        String a = pol.replaceAll("-", "+-");
        String[] parts = a.split("\\+");

        for(String parte:parts){

            if(!parte.equals("")){
                String[] parts2;
                parts2=parte.split("x\\^");
                for(String s:parts2){
                    if(s.contains("x")){
                        String[] x=s.split("x");
                        if(x.length==0){
                            c = 1;
                            p = 1;
                        }else if(x.length==1 && x[0].equals("-")){
                            c = -1;
                            p = 1;
                        }else{
                            c = Integer.parseInt(x[0]);
                            p = 1;
                        }
                    }else if(parts2.length==1){
                        c = Integer.parseInt(parts2[0]);
                        p = 0;
                    }
                    else if(parts2.length == 2 && parts2[0].equals("")) {
                        c = 1;
                        p = Integer.parseInt(parts2[1]);
                    }
                    else if(parts2.length == 2 && parts2[0].equals("-")){
                        c = -1;
                        p = Integer.parseInt(parts2[1]);
                    }
                    else if(parts2.length == 2 && parts2[1].contains("-")){

                    }
                    else{
                        c = Integer.parseInt(parts2[0]);
                        p = Integer.parseInt(parts2[1]);
                    }
                }
            }
            if(c != 0)
            {
                list.add(new Monomial(p, c));
            }
        }
    }

    public Polynomial(Monomial one){
       list = new ArrayList<Monomial>();
        this.list.add(one);
    }

    public int getGrade(){
        int maxg = 0;

        for(Monomial i: list){
            if(i.getExp() >= maxg)
                maxg = i.getExp();
        }
        return maxg;
    }

    public Monomial getTermMaxGrade(){
        int maxg = 0;
        maxg = this.getGrade();
        Monomial aux = new Monomial();

        for(Monomial i: this.list){
            if(i.getExp() == maxg){
                aux = new Monomial(i.getExp(), i.getCoef());
            }
        }

        return aux;
    }

    public ArrayList<Monomial> getList() {
        return this.list;
    }

    public void addTerm(Monomial m){
        if(m.getCoef()!=0){
            list.add(m);
        }

    }

    public String toString(){
        String s = "";

        for(Monomial i: list) {
            if(i.getCoef() != 0){
                if(i != list.get(list.size() - 1)){
                    s += i.toString() + "+";
                }
                else {
                    s += i.toString();
                }
            }

        }
        return s;
    }
}
