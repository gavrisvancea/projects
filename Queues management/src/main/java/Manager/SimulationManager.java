package Manager;

import Model.*;
import Strategy.*;

import java.io.*;
import java.util.*;

public class SimulationManager implements Runnable {

    public int time;
    private int minProcessingTime;
    private int maxProcessingTime;
    private int minArrivalTime;
    private int maxArrivalTime;
    private int nbOfServers;
    private int nbOfClients;
    private SelectionPolicy select;
    private Scheduler sched;
    private ArrayList<Task> randGeneratedClients;
    private volatile boolean flag = true;
    FileWriter out;

    public SimulationManager(String file_in, String file_out){
        File fil = new File(file_in);
        try {
            Scanner in = new Scanner(fil);
            while (in.hasNextInt()) {
                this.nbOfClients = in.nextInt();
                this.nbOfServers = in.nextInt();
                this.time = in.nextInt();
                this.minArrivalTime = in.nextInt();
                this.maxArrivalTime = in.nextInt();
                this.minProcessingTime = in.nextInt();
                this.maxProcessingTime = in.nextInt();
            }
        }
        catch(Exception e){
            e.getMessage();
        }

        try {
            out = new FileWriter(file_out);
        }
        catch (Exception e){
            e.getMessage();
        }

        this.select = SelectionPolicy.SHORTEST_TIME;
        this.generateRandomTasks();

        this.sched = new Scheduler(nbOfServers, select);
    }

    private void generateRandomTasks(){

        Random generator = new Random();
        this.randGeneratedClients = new ArrayList<Task>();
        for(int i = 0; i < nbOfClients; i++){
            int arrival = generator.nextInt(maxArrivalTime - minArrivalTime) + minArrivalTime;
            int process = generator.nextInt(maxProcessingTime - minProcessingTime) + minProcessingTime;

            Task client = new Task(i+1, arrival, process);
            this.randGeneratedClients.add(client);

        }
        Collections.sort(this.randGeneratedClients);
    }

    public void write(int time, FileWriter f){
        String tim = "Time: " + time;
        String s = "Waiting clients: ";
        int r = 1;
        try{
            f.write(tim);
            f.write('\n');
            for(Task c: randGeneratedClients){
               s += c + " ";
            }
            f.write(s);
            f.write('\n');
            for(Server i: sched.getQueues()){
                String coada = "Queue" + r + ": ";
                r++;
                if(!i.getClients().isEmpty()){
                    for(Task client: i.getClients()){
                        coada += client;
                    }
                }
                else{
                    coada += " closed";
                }
                f.write(coada);
                f.write('\n');
            }
        }
        catch(Exception e){
            e.getMessage();
        }
    }

    public void closeFile(FileWriter f){
        try {
            f.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public double averageTime(){
        double avg = 0;
        int unprocessedClients = 0;
        for(Server i: sched.getQueues()){
            avg += i.getWaitingTime();
            unprocessedClients += i.getClients().size();
        }
        return avg/(double) (nbOfClients - unprocessedClients);
    }

    public String averageTimeString(){
        String s = "";
        return s + this.averageTime();
    }

    public void writeAverage(){
        try {
            this.out.write("\n");
            String s = "Average time: " + this.averageTimeString();
            this.out.write(s);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void run() {
        int currTime = 0;
        while(currTime < time && flag){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            ArrayList<Task> copy = (ArrayList<Task>) this.randGeneratedClients.clone();
            for(Task t: copy){
                if(t.getArrivalTime() == currTime){
                    sched.dispatchClient(t);
                    randGeneratedClients.remove(t);
                }
            }
            this.write(currTime, out);
            ArrayList<Server> aux = (ArrayList<Server>) sched.getQueues().clone();
            if(randGeneratedClients.isEmpty()){
                for(Server i: sched.getQueues()){
                    if(i.getClients().isEmpty()){
                        aux.remove(i);
                    }
                }
            }
            if(aux.isEmpty()){
                this.writeAverage();
                closeFile(out);
                flag = false;
           }
            currTime++;
        }
    }

    public static void main(String args[]) throws FileNotFoundException {

        //SimulationManager s = new SimulationManager( "in-test-1.txt", "out-test-1.txt");
        SimulationManager s = new SimulationManager( args[0], args[1]);
        Thread t=new Thread(s);
        t.start();
    }
}
