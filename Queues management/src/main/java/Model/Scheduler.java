package Model;

import Strategy.*;

import java.util.*;

public class Scheduler {

    private ArrayList<Server> servers;
    private int maxNbofServers;
    private int maxClientsPerServer;
    private Strategy strategy;

    public Scheduler() {
        servers = new ArrayList<Server>();
    }

    public Scheduler(int serv, SelectionPolicy s) {

        servers = new ArrayList<Server>();
        maxNbofServers = serv;
        maxClientsPerServer = 1000;
        for(int i = 0; i < maxNbofServers; i ++){
            servers.add(new Server());
        }
        this.changeStrategy(s);
    }

    public void changeStrategy(SelectionPolicy select) {

        if (select == SelectionPolicy.SHORTEST_QUEUE) {
            strategy = new ConcreteStrategyQueue();
        }
        if (select == SelectionPolicy.SHORTEST_TIME) {
            strategy = new ConcreteStrategyTime();
        }
    }

    public void dispatchClient(Task client) {
        this.strategy.addClient(servers, client);
    }

    public ArrayList<Server> getQueues() {
        return this.servers;
    }

}
