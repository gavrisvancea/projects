package Strategy;

import Model.Server;
import Model.Task;

import java.util.*;

public interface Strategy {

    public void addClient(ArrayList<Server> servers, Task client);
}
