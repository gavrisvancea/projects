package Strategy;

import Model.Server;
import Model.Task;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import Model.*;
public class ConcreteStrategyTime implements Strategy {

    public void addClient(ArrayList<Server> servers, Task client){
        int mintime = 9999;
        int time = 0;
        Server aux = new Server();
        for(Server i:servers){
            if(i.getPeriod().get() < mintime){
                mintime = i.getPeriod().get();
                aux = i;
            }
        }
        aux.addClient(client);
    }

}
