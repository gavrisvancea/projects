package bll;

import dao.ClientDAO;
import model.Client;

import java.util.ArrayList;
import java.util.List;

/**
 * Clasa care realizeaza logica de executare a comenzilor SQl pentru tabelul client
 */
public class ClientBLL {

    private ClientDAO clientDB;

    /**
     * Constructor care initializeaza un obiect de tipul
     * ClientDAO pentru apelarea comenzilorSQL
     */
    public ClientBLL(){
        this.clientDB = new ClientDAO();
    }

    /**
     * cauta un client dupa ID
     * @param id ID-ul dupa care se face cautarea
     * @return returneaza Clientul care are ID-ul dat
     */
    public Client findById(int id){
        try {
            return this.clientDB.findById(id);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * cauta un client dupa nume
     * @param name numele dupa care se face cautarea
     * @return returneaza Clientul care are numele dat
     */
    public Client findByName(String name){
        return this.clientDB.findByName(name);
    }

    /**
     * insereaza un client nou in tabelul clienti daca nu
     * a fost gasit numele acestuia printre restul clientilor
     * @param c clientul care trebuie inserat
     * @return returneaza 0 daca s-a inserat si 1 daca nu s-a inserat
     */
    public int insertClient(Client c){
        int ok = 0;
        List<Client> lst = this.clientDB.report();
        for(Client i: lst){
            if(i.getName().equals(c.getName())){
                ok = 1;
            }
        }
        if(ok == 0){
            this.clientDB.insert(c);
        }
        return ok;
    }

    /**
     * sterge un client dupa ID-ul dat
     * @param id ID-ul dupa care se face stergerea
     * @return returneaza 1 daca stergerea s-a realizat si 0 daca nu s-a sters
     */
    public int deleteClient(int id){
        int ok = 0;
        List<Client> lst = this.clientDB.report();
        for(Client i: lst){
            if(i.getIdClient() == id){
                ok = 1;
            }
        }
        if(ok == 1){
            this.clientDB.deleteClient(id);
        }
        return ok;
    }

    /**
     * modifica adresa unui client
     * @param newAddress noua adresa atribuita
     * @param id ID-ul clientului la care se face update
     * @return returneaza 1 daca s-a facut update si 0 daca nu s-a facut update
     */
    public int updateAddress(String newAddress, int id){
        int ok = 0;
        List<Client> lst = this.clientDB.report();
        for(Client i: lst){
            if(i.getIdClient() == id){
                ok = 1;
            }
        }
        try {
            if (ok == 1) {
                Client newClient = new Client(clientDB.findById(id).getName(), newAddress);
                this.clientDB.update(newClient, id);
            }
        }catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return ok;
    }

    /**
     * inlocuirea unui client cu unul nou
     * @param c noul client
     * @param id ID-ul clientului care trebuie schimbat
     * @return returneaza 1 daca s-a facut update ;i 0 daca nu s-a facut update
     */
    public int updateClient(Client c, int id){
        int ok = 0;
        List<Client> lst = this.clientDB.report();
        for(Client i: lst){
            if(i.getIdClient() == id){
                ok = 1;
            }
        }
        if(ok == 1){
            this.clientDB.update(c, id);
        }
        return ok;
    }

    /**
     * accesarea listei de clienti din tabelul client
     * @return returneaza lista de clienti din baza de date
     */
    public List<Client> reportClient(){
        return this.clientDB.report();
    }
}
