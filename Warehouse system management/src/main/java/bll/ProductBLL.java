package bll;

import dao.ProductDAO;
import model.Client;
import model.Product;

import java.util.List;

/**
 * Clasa care realizeaza logica de executare a comenzilor SQl pentru tabelul product
 */
public class ProductBLL {

    private ProductDAO productDB;

    /**
     * Constructor care initializeaza un obiect de tipul
     * ProductDAO pentru apelarea comenzilor SQL
     */
    public ProductBLL(){
        this.productDB = new ProductDAO();
    }

    /**
     * insereaza un produs nou in depozit sau
     * updateaza cantitatea daca produsul exista deja in depozit
     * @param prod produsul care trebuie inserat
     * @return returneaza 1 daca se face update si 0 daca se insereaza produsul
     */
    public int insertProduct(Product prod){
        int ok = 0;
        List<Product> lst = this.productDB.report();
        int id = 0;
        for(Product p: lst){
            if(prod.getProductName().equals(p.getProductName())){
                ok = 1;
                id = p.getIdProduct();
            }
        }
        if(ok == 1){
            Product pr = new Product(prod.getProductName(), prod.getPrice(), productDB.findByName(prod.getProductName()).getQuantity() + prod.getQuantity());
            this.productDB.update(pr, id);
        }
        else{
            this.productDB.insert(prod);
        }
        return ok;
    }

    /**
     * sterge un produs dupa ID
     * @param id ID-ul dupa care se face stergerea
     * @return returneaza 1 daca s-a realizat stergerea si 0 daca nu
     */
    public int deleteProduct(int id){
        int ok = 0;
        List<Product> lst = this.productDB.report();
        for(Product i: lst){
            if(i.getIdProduct() == id){
                ok = 1;
            }
        }
        if(ok == 1){
            this.productDB.deleteProduct(id);
        }
        return ok;
    }

    /**
     * cauta un produs in baza de date dupa numele acestuia
     * @param name numele dupa care se face cautarea
     * @return returneaza produsul din baza de date care are numele cautat
     */
    public Product findByName(String name){
        return this.productDB.findByName(name);
    }

    /**
     * cauta un produs dupa ID
     * @param id ID-ul dupa care se face cautarea
     * @return returneaza produsul din baza de date care are ID-ul cautat
     */
    public Product findById(int id){
        try {
            return this.productDB.findById(id);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * accesarea listei de produse din tabelul product
     * @return returneaza lista de produse din baza de date
     */
    public List<Product> reportProduct(){
        return this.productDB.report();
    }
}
