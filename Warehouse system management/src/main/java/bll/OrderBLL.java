package bll;

import dao.ClientDAO;
import dao.OrderDAO;
import model.Client;
import model.Order;


import java.util.List;

/**
 * Clasa care realizeaza logica de executare a comenzilor SQl pentru tabelul order
 */
public class OrderBLL {

    private OrderDAO orderDB;

    /**
     * Constructor care initializeaza un obiect de tipul
     * OrderDAO pentru apelarea comenzilorSQL
     */
    public OrderBLL(){
        this.orderDB = new OrderDAO();
    }

    /**
     * inserarea unei comenzi noi pentru un client
     * @param ord comanda care trebuie inserata
     * @return returneaza 1 daca s-a facut inserarea si 0 daca nu
     */
    public int insert(Order ord){
        int ok = 0;
        List<Client> clients = (new ClientDAO()).report();
        for(Client i: clients){
            if(ord.getIdClient() == i.getIdClient()){
                ok = 1;
            }
        }
        for(Order i: this.orderDB.report()){
            if(i.getIdClient() == ord.getIdClient()){
                ok = 0;
            }
        }
        if(ok == 1){
            this.orderDB.insert(ord);
        }
        return ok;
    }

    /**
     * stergerea unei comenzi dupa ID
     * @param id ID-ul dupa care se face stergerea
     * @return returneaza 1 daca s-a realizat stergerea si 0 daca nu
     */
    public int deleteOrder(int id){
        int ok = 0;
        List<Order> lst = this.orderDB.report();
        for(Order i: lst){
            if(i.getIdorder() == id){
                ok = 1;
            }
        }
        if(ok == 1){
            this.orderDB.deleteOrder(id);
        }
        return ok;
    }

    /**
     * cauta o comanda dupa ID-ul clientului
     * @param idClient ID-ul clientului dupa care se face cautarea
     * @return returneaza comanda care are ID-ul clientului egal cu parametrul
     */
    public Order findById(int idClient){
        return this.orderDB.findByIdClient(idClient);
    }

    /**
     * cauta o comanda dupa ID
     * @param id ID-ul dupa care se face cautarea
     * @return returneaza comanda care are ID-ul egal cu cel dat
     */
    public Order findByIdOrd(int id){
        try {
            return this.orderDB.findById(id);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * accesarea listei de comenzi din tabelul order
     * @return returneaza lista de comenzi din baza de date
     */
    public List<Order> reportOrders(){
        return this.orderDB.report();
    }
}
