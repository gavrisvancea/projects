package model;

/**
 * Clasa care abstractizeaza tabelul Client din baza de date
 */
public class Client {

    private int idClient;
    private String name;
    private String address;

    /**
     * Constructor fara parametri care initializeaza un client nou
     */
    public Client(){

    }

    /**
     * Constructor care creeaza un client nou
     * @param name numele care se atribuie noului client
     * @param address adresa care se atribuie noului client
     */
    public Client(String name, String address){
        this.name = name;
        this.address = address;
    }

    /**
     * Getter pentru ID
     * @return returneaza ID-ul clientului
     */
    public int getIdClient() {
        return idClient;
    }

    /**
     *Setter pentru ID
     * @param id noul ID care se atribuie
     */
    public void setIdClient(int id) {
        this.idClient = id;
    }

    /**
     * Getter pentru nume
     * @return returneaza numele clientului
     */
    public String getName() {
        return name;
    }

    /**
     * Setter pentru nume
     * @param name noul nume care se atribuie
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter pentru adresa
     * @return returneaza adresa clientului
     */
    public String getAddress() {
        return address;
    }

    /**
     * Setter pentru adresa
     * @param address noua adresa atribuita clientului
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Transforma in String obiectul de tip Client
     * @return returneaza reprezentarea in forma de String
     */
    public String toString(){
        String s = " ";

        return this.getIdClient() + s + this.getName() + s + this.getAddress();
    }
}
