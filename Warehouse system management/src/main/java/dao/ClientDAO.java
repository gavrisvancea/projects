package dao;

import connection.ConnectionFactory;
import model.Client;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Clasa care mosteneste AbstractDAO si are ca parametru generic clasa Client care face
 * abstractizarea tabelului client din baza de date
 */
public class ClientDAO extends AbstractDAO<Client> {

    private final static String NAME_FIND = "SELECT * FROM gavris.CLIENT WHERE name = ?";
    private final static String DELETE = "DELETE FROM gavris.client WHERE idClient = ?";

    /**
     * se face cautarea in baza de date dupa numele clientului
     * @param clientName numele dupa care se face cautarea
     * @return returneaza clientul afisat in urma cautarii
     */
    public Client findByName(String clientName){

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try{
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(NAME_FIND);
            statement.setString(1, clientName);
            resultSet = statement.executeQuery();
            return super.createObjects(resultSet).get(0);

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    /**
     * sterge un client dupa un ID dat
     * @param id ID-ul dupa care se face stergerea
     */
    public void deleteClient(int id){

        Connection connection = null;
        PreparedStatement statement = null;
        try{
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(DELETE);
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
    }
}
