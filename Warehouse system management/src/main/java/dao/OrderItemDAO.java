package dao;

import connection.ConnectionFactory;
import model.OrderItem;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Clasa care mosteneste AbstractDAO si are ca parametru generic clasa OrderItem care face
 * abstractizarea tabelului orderItem din baza de date
 */
public class OrderItemDAO extends AbstractDAO<OrderItem> {
    private final static String ID_PROD = "SELECT * FROM gavris.orderitem WHERE idProduct = ?";
    private final static String DELETE = "DELETE FROM gavris.orderitem WHERE idorderItem = ?";

    /**
     * se face cautarea unui produs comandat dupa ID-ul produsului din depozit
     * @param id ID-ul produsului din depozit
     * @return returneaza produsul comandat rezultat in urma cautarii
     */
    public OrderItem findByProdId(int id){
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try{
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(ID_PROD);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            return super.createObjects(resultSet).get(0);

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    /**
     * stergerea unui produs comandat din baza de date dupa ID
     * @param id ID-ul dupa care se face stergerea
     */
    public void deleteOrderItem(int id){
        Connection connection = null;
        PreparedStatement statement = null;
        try{
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(DELETE);
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
    }
}
