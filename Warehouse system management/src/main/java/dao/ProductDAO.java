package dao;

import connection.ConnectionFactory;
import model.Product;

import java.sql.*;

/**
 * Clasa care mosteneste AbstractDAO si are ca parametru generic clasa Product care face
 * abstractizarea tabelului product din baza de date
 */
public class ProductDAO extends AbstractDAO<Product>{

    private final static String NAME_FIND = "SELECT * FROM gavris.product WHERE productName = ?";
    private final static String DELETE = "DELETE FROM gavris.product WHERE idProduct = ?";

    /**
     * cauta un produs in depozit dupa numele acestuia
     * @param prodName numele produsului dupa care se face cautarea
     * @return returneaza produsul rezultat in urma cautarii
     */
    public Product findByName(String prodName){
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(NAME_FIND);
            statement.setString(1, prodName);
            resultSet = statement.executeQuery();
            return super.createObjects(resultSet).get(0);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    /**
     * stergerea unui produs din depozit dupa ID
     * @param id ID-ul dupa care se realizeaza stergerea
     */
    public void deleteProduct(int id){
        Connection connection = null;
        PreparedStatement statement = null;
        try{
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(DELETE);
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
    }
}
